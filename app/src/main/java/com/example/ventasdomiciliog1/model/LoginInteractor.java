package com.example.ventasdomiciliog1.model;

import com.example.ventasdomiciliog1.mvp.LoginMVP;

import java.util.HashMap;
import java.util.Map;

public class LoginInteractor implements LoginMVP.Model {

    //Crear usuarios simulando la base de datos
    private Map<String, String> users; //Diccionario de java

    public LoginInteractor() {
        users = new HashMap<>();
        users.put("vivissalamanca@hotmail.com", "Yuvisamar");
    }

   /*
    @Override
    //Metodo de validación de usuario-contraseña correctas
 public boolean validationCredentials(String email, String password) {
        //Funcionalidad de validación
        return users.get(email) != null
                && users.get(email).equals(password);
        }
    */

    @Override
    public void validationCredentials(String email, String password, ValidateCredentialsCallback callback) {
        //En caso de haber demora al cargar
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(users.get(email) == null) {
            callback.onFailure("Usuario no existe");
        } else if (!users.get(email).equals(password)){
            callback.onFailure("Contraseña incorrecta");
        } else {
            callback.onSuccess();
        }

    }
}
