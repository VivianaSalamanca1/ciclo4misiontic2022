package com.example.ventasdomiciliog1.model;

import com.example.ventasdomiciliog1.mvp.PaymentsMVP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PaymentsInteractor implements PaymentsMVP.Model {

    //Simular los datos de la base de datos
    private List<PaymentsMVP.PaymentsInfo> data;
    //Constructor
    public PaymentsInteractor() {
        data = Arrays.asList(
                new PaymentsMVP.PaymentsInfo("Vivis", "Cra 1 # 2 - 03"),
        new PaymentsMVP.PaymentsInfo("Sebas", "Cra 27 # 24 - 05")
        );
    }

    @Override
    public void loadPayments(LoadPaymentsCallback callback) {
        try {
            Thread.sleep(5000); //En caso de demora al cargar datos
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        //callback.showPaymentsInfo(new ArrayList<>());//Por defecto devuelve una lista vacía
        callback.showPaymentsInfo(this.data);
    }
}
