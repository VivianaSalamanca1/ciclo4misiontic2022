package com.example.ventasdomiciliog1.mvp;

import android.app.Activity;

import java.util.List;

public interface NewSaleMVP {

    interface Model{
    }

    interface Presenter { //Captura los eventos de la ventana
        void saveSale(); //botón guardar venta
    }

    interface View {//Operaciones o interacciones con la vista
        //Se requiere obtener la información
        NewSaleMVP.NewSaleInfo getNewSaleInfo();//Obtener datos
        Activity getActivity();
        void onDateClick();
        void clearDate(); //Borrar datos de los campos
        void message(); //Mensaje de datos guardados
        void messageIncomplete(); //Mensaje de campos sin llenar
    }
    class NewSaleInfo{

        private String tittle;
        private String client;
        private String address;
        private String amount;
        private String number;
        private String periodicity;
        private String part;
        private String date;

        //Constructor
        public NewSaleInfo(String tittle, String client, String address, String amount, String number, String periodicity, String part, String s) {
            this.tittle = tittle;
            this.client = client;
            this.address = address;
            this.amount = amount;
            this.number = number;
            this.periodicity = periodicity;
            this.part = part;
            //this.date = date;
        }

        //Get
        public String getTittle() { return tittle; }
        public String getClient() { return client; }
        public String getAddress() { return address; }
        public String getAmount() { return amount; }
        public String getNumber() { return number; }
        public String getPeriodicity() { return periodicity; }
        public String getPart() { return part; }
        public String getDate() { return date; }
    }
}
