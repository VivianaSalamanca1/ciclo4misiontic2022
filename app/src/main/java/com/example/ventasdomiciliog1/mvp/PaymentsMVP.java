package com.example.ventasdomiciliog1.mvp;

import android.app.Activity;

import java.util.List;

public interface PaymentsMVP {
    interface Model{
        void loadPayments(LoadPaymentsCallback callback);

        interface LoadPaymentsCallback {
            void showPaymentsInfo(List<PaymentsInfo> paymentsInfo);

        }
    }

    interface Presenter { //Captura los eventos de la ventana
        void loadPayments();
        void onNewSaleClick();

    }

    interface View {

        Activity getActivity();

        void showProgressBar(); //Mostrar barra de progreso

        void hideProgressBar(); //Ocultar barra de progreso

        void showNewSaleActivity();

        void showPaymentsInfo(List<PaymentsInfo> paymentsInfo); //Pasar información

        void showGeneralMessage(String credenciales_invalidas);
    }

    class  PaymentsInfo {
        private String image;
        private String name;
        private String address;

        //Constructores
        public PaymentsInfo(String name, String address) {
            this(null, name, address);
        }
        public PaymentsInfo(String image, String name, String address) {
            this.image = image;
            this.name = name;
            this.address = address;
        }
        //Método Get
        public String getImage() {
            return image;
        }
        public String getName() {
            return name;
        }
        public String getAddress() {
            return address;
        }
    }
}
