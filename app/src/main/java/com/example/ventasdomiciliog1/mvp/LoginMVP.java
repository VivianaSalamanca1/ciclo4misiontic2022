package com.example.ventasdomiciliog1.mvp;

import android.app.Activity;

public interface LoginMVP {

    interface Model {
        void validationCredentials(String email, String password, ValidateCredentialsCallback callback);

        interface ValidateCredentialsCallback {
            void onSuccess();
            void onFailure(String error);
        }
    }
    interface Presenter { //Captura los eventos de la ventana
        void isLogged();

        //Eventos al darle click en los botones de la ventana

        void loginwithEmail(); //Al darle click en iniciar sesión

        void loginwithFacebook(); //Al darle click en facebook

        void loginwithGmail(); //Al darle click en Google
    }
    interface View { //Operaciones o interacciones con la vista
        Activity getActivity();

        LoginInfo getLoginInfo(); //Se requiere obtener la información

        void showEmailError(String error); //Mostrar cuando hay error al digitar el email

        void showPasswordError(String error); //Mostrar cuando hay error al digitar la contraseña

        void showGeneralMessage(String error); //Mostrar cuando hay error general en la ventana (Ej: con el usuario)

        void clearDate(); //Borrar datos de los campos

        void openPaymentsActivity(); //Abrir una nueva activity

        void startWaiting(); //Iniciar espera del thread implementado en el doc Presenter

        void stopWaiting(); //Parar la espera del thread implementado en el doc Presenter
    }
    class LoginInfo{
        //Atributos
        private String email;
        private String password;
        //Constructor
        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }
        //Get
        public String getEmail() {
            return email;
        }
        public String getPassword() {
            return password;
        }
    }
}
