package com.example.ventasdomiciliog1.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ventasdomiciliog1.R;
import com.example.ventasdomiciliog1.mvp.LoginMVP;
import com.example.ventasdomiciliog1.mvp.NewSaleMVP;
import com.example.ventasdomiciliog1.presenter.NewSalePresenter;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class NewSaleActivity extends AppCompatActivity implements NewSaleMVP.View {

    private NewSaleMVP.Presenter presenter;

    private TextView tvTittle;
    private TextInputLayout tilClient;
    private TextInputEditText etClient;
    private TextInputLayout tilAddress;
    private TextInputEditText etAddress;
    private TextInputLayout tilAmount;
    private TextInputEditText etAmount;
    private TextInputLayout tilNumber;
    private TextInputEditText etNumber;
    private TextInputLayout tilPeriodicity;
    private MaterialAutoCompleteTextView etPeriodicity;
    private TextInputLayout tilPart;
    private TextInputEditText etPart;
    private TextInputLayout tilDate;
    private TextInputEditText etDate;
    private AppCompatButton btnSave;

    private Date selectedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sale);

        initUI();
        presenter = new NewSalePresenter(this);
    }

    @SuppressLint("WrongViewCast")
    private void initUI(){

        tvTittle = (TextView) findViewById(R.id.tv_tittle);

        tilClient = findViewById(R.id.til_client);
        etClient = findViewById(R.id.et_client);

        tilAddress = findViewById(R.id.til_address);
        etAddress = findViewById(R.id.et_address);

        tilAmount = findViewById(R.id.til_amount);
        etAmount = findViewById(R.id.et_amount);

        tilNumber = findViewById(R.id.til_number);
        etNumber = findViewById(R.id.et_number);

        tilAddress = findViewById(R.id.til_address);
        etAddress = findViewById(R.id.et_address);

        tilPeriodicity = findViewById(R.id.til_periodicity);
        etPeriodicity = findViewById(R.id.et_periodicity);

        tilPart = findViewById(R.id.til_part);
        etPart = findViewById(R.id.et_part);

        tilDate = findViewById(R.id.til_date);
        tilDate.setEndIconOnClickListener(v -> onDateClick());

        etDate = findViewById(R.id.et_date);

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener((evt) -> presenter.saveSale());
    }

    public void onDateClick(){
        //Selección de fecha actual
        if(selectedDate==null){
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            cal.add(Calendar.DAY_OF_MONTH, 1); //Seleccionar el día después de la fecha actual
            selectedDate = new Date(); //Seleccionar la fecha actual
            selectedDate = cal.getTime();
        }


        //Restricción de fecha (opción de seleccionar desde día actual)
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        CalendarConstraints constraints = new CalendarConstraints.Builder()
                //.setValidator(DateValidatorPointForward.from(cal.getTimeInMillis()))//Desde el día siguiente
                .setValidator(DateValidatorPointForward.now())//Desde la fecha actual
                .build();

        //Patrón de diseño constructor
        MaterialDatePicker<Long> datePicker= MaterialDatePicker.Builder
                .datePicker()
                .setSelection(selectedDate.getTime())
                .setCalendarConstraints(constraints)
                .setTitleText(R.string.newsale_date)
                .build();
        datePicker.addOnPositiveButtonClickListener(this::onDateSelected);
        datePicker.show(getSupportFragmentManager(), "date");
    }

    private void onDateSelected(Long selection){
        selectedDate = new Date(selection);

        etDate.setText(SimpleDateFormat.getDateInstance().format(selectedDate));
    }

    public Activity getActivity(){
        return this;
    }

    @Override
    public NewSaleMVP.NewSaleInfo getNewSaleInfo() {
        return new NewSaleMVP.NewSaleInfo(tvTittle.getText().toString() ,etClient.getText().toString(), etAddress.getText().toString(), etAmount.getText().toString(), etNumber.getText().toString(), etPeriodicity.getText().toString(), etPart.getText().toString(), etDate.getText().toString());
    }

    @Override
    public void clearDate() {
        tilClient.setError("");
        etClient.setText("");
        tilAddress.setError("");
        etAddress.setText("");
        tilAmount.setError("");
        etAmount.setText("");
        tilNumber.setError("");
        etNumber.setText("");
        tilPeriodicity.setError("");
        etPeriodicity.setText("");
        tilPart.setError("");
        etPart.setText("");
        etDate.setText("");
    }

    @Override
    public void message() {
        Toast.makeText(NewSaleActivity.this,"Guardado exitosamente", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void messageIncomplete() {
        Toast.makeText(NewSaleActivity.this,"Obligatorio llenar todos los campos", Toast.LENGTH_SHORT).show();
    }

}
