package com.example.ventasdomiciliog1.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.app.Activity;

import com.example.ventasdomiciliog1.R;
import com.example.ventasdomiciliog1.mvp.LoginMVP;
import com.example.ventasdomiciliog1.presenter.LoginPresenter;
import com.example.ventasdomiciliog1.view.PaymentsActivity;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LogoActivityManual extends AppCompatActivity implements LoginMVP.View {

    //Atributos (iv y et basados en como le haya colocado en la activity)
    //Variables de persistencia
    private final static String EMAIL_KEY = "email";
    private final static String PASSWORD_KEY = "password";

    private LinearProgressIndicator piWaiting;
    private ImageView ivLogo;
    private TextInputLayout tilCorreo;
    private TextInputEditText etCorreo;
    private TextInputLayout tilClave;
    private TextInputEditText etClave;

    private AppCompatButton btnIniciar;
    private AppCompatButton btnFacebook;
    private AppCompatButton btnGoogle;

    private LoginMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo_manual);

        presenter = new LoginPresenter(this);
        presenter.isLogged();

        //Funcionalidad de cada elemento (atributo)
        initUI();
    }

    //PERSISTENCIA
    //Guardar información
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        outState.putString(EMAIL_KEY, etCorreo.getText().toString());
        outState.putString(PASSWORD_KEY, etClave.getText().toString());

        super.onSaveInstanceState(outState, outPersistentState);
    }
    //Recuperar información
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        etCorreo.setText(savedInstanceState.getString(EMAIL_KEY));
        etClave.setText(savedInstanceState.getString(PASSWORD_KEY));
    }

    //Método de inicialización
    private void initUI() {
        piWaiting = findViewById(R.id.pi_waiting);

        ivLogo = findViewById(R.id.iv_logo); //Buscar y traer el atributo ivLogo en el activity indicado

        tilCorreo= findViewById(R.id.til_correo);
        etCorreo = findViewById(R.id.et_correo);
        //etCorreo = findViewById(R.id.et_correo); //Buscar y traer el atributo etCorreo en el activity indicado
        //etCorreo.setText("cdiaz@example.com"); //Asignar automáticamente información deseada en componente indicado

        tilClave= findViewById(R.id.til_clave);
        etClave = findViewById(R.id.et_clave);
        //etClave = findViewById(R.id.et_clave); //Buscar y traer el atributo etClave en el activity indicado
        //etClave.setText("Abcd1234"); //Asignar automáticamente información deseada en componente indicado

        //Interconexión entre interfaces (botones)
        btnIniciar = findViewById(R.id.btn_iniciar);
        btnIniciar.setOnClickListener((evt) -> presenter.loginwithEmail());
            //etCorreo.setText("");
            //etClave.setText("");
            //Toast.makeText( this, "Inicio de sesion correcto", Toast.LENGTH_SHORT).show(); //En los parentesis se debe escribir primero this, "" y después se completa lo faltante
            //});

        btnFacebook= findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener((evt) -> presenter.loginwithFacebook());

        btnGoogle= findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener((evt) -> presenter.loginwithGmail());
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(etCorreo.getText().toString(), etClave.getText().toString());
    }
    @Override
    public void showEmailError(String error) {
        tilCorreo.setError(error);
    }
    @Override
    public void showPasswordError(String error) {
        tilClave.setError(error);
    }
    @Override
    public void showGeneralMessage(String error) {
        Toast.makeText(LogoActivityManual.this, error, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void clearDate() {
        tilCorreo.setError("");
        etCorreo.setText("");
        tilClave.setError("");
        etClave.setText("");
    }
    @Override
    public void openPaymentsActivity() {
        Intent intent= new Intent(this, PaymentsActivity.class); //Botón de interconexión entre interfaces
        startActivity(intent);

        getBaseContext();
    }

    @Override
    public void startWaiting() {
        piWaiting.setVisibility(View.VISIBLE);
        //Deshailitar botones
        btnIniciar.setEnabled(false);
        btnFacebook.setEnabled(false);
        btnGoogle.setEnabled(false);
    }

    @Override
    public void stopWaiting() {
        piWaiting.setVisibility(View.GONE);
        //Habilitar botones
        btnIniciar.setEnabled(true);
        btnFacebook.setEnabled(true);
        btnGoogle.setEnabled(true);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    //Métodos implementados en el MVP presenter (están arriba en este archivo)
    //Acciones de los botones
    /*
    private void onLoginClick(){
        //Se pasó a los métodos implementados del MVP (están más abajo)
            Intent intent= new Intent(this, PaymentsActivity.class); //Botón de interconexión entre interfaces
            startActivity(intent);
            //etCorreo.setText("");
            //etClave.setText("");
            //Toast.makeText( this, "Inicio de sesion correcto", Toast.LENGTH_SHORT).show(); //En los parentesis se debe escribir primero this, "" y después se completa lo faltante
    }
    private void onFacebookClick(){
        //Se pasó a los métodos implementados del MVP (están más abajo)
            Intent intent= new Intent(this, PaymentsActivity.class); //Botón de interconexión entre interfaces
            startActivity(intent);
            }
    private void onGoogleClick(){
        //Se pasó a los métodos implementados del MVP (están más abajo)
            Intent intent= new Intent(this, PaymentsActivity.class); //Botón de interconexión entre interfaces
            startActivity(intent);
            }
     */
}