package com.example.ventasdomiciliog1.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ventasdomiciliog1.R;
import com.example.ventasdomiciliog1.mvp.PaymentsMVP;

import java.util.ArrayList;
import java.util.List;

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

    private List<PaymentsMVP.PaymentsInfo> data;

    //Constructores
    public PaymentsAdapter() {
        this.data = new ArrayList<>();
    }
    public void setData(List<PaymentsMVP.PaymentsInfo> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    //Crear un nuevo objeto por cada uno de los elementos que se tenga en la pantalla
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_payments, parent, false);

        return new ViewHolder(view);
    }
    //Asociar el método ViewHolder con los datos
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaymentsMVP.PaymentsInfo item = data.get(position);

        //TODO: holder.getTvClient().setImageIcon();
        holder.getTvClient().setText(item.getName());
        holder.getTvAddress().setText(item.getAddress());
    }
    //Decir cuantos elementos tiene la vista
    @Override
    public int getItemCount() {
        return data.size();
    }
    //Convertir objetos de una lista en elementos visibles dentro de la pantalla
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivClient;
        private TextView tvClient;
        private TextView tvAddress;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            //(Función Clickeable) Falta copiar en el cuaderno
            itemView.setOnClickListener( v-> {
                Toast.makeText(itemView.getContext(), tvClient.getText().toString(), Toast.LENGTH_SHORT).show();
            });

            ivClient = itemView.findViewById(R.id.iv_client);
            tvClient = itemView.findViewById(R.id.tv_client);
            tvAddress = itemView.findViewById(R.id.tv_address);
        }
        //Get
        public ImageView getIvClient() {
            return ivClient;
        }
        public TextView getTvClient() {
            return tvClient;
        }
        public TextView getTvAddress() {
            return tvAddress;
        }
    }
}

