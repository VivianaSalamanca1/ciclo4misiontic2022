package com.example.ventasdomiciliog1.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.ventasdomiciliog1.R;
import com.example.ventasdomiciliog1.mvp.PaymentsMVP;
import com.example.ventasdomiciliog1.presenter.PaymentsPresenter;
import com.example.ventasdomiciliog1.view.adapter.PaymentsAdapter;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.List;

public class PaymentsActivity extends AppCompatActivity implements PaymentsMVP.View {

    private DrawerLayout drawerLayout;
    private MaterialToolbar appBar;
    private NavigationView navigationDrawer;

    private LinearProgressIndicator piWaiting;
    private RecyclerView rvPayments;
    private FloatingActionButton btnSale;

    private PaymentsMVP.Presenter presenter;
    private PaymentsAdapter paymentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        presenter = new PaymentsPresenter(this); //Vinculación con interfaz presentador

        initUI();

        presenter.loadPayments(); //Método de cargar pagos

    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(menuItem -> navigationItemSelected(menuItem));

        piWaiting= findViewById(R.id.pi_waiting);

        rvPayments = findViewById(R.id.rv_payments);
        rvPayments.setLayoutManager(new LinearLayoutManager(PaymentsActivity.this));
        paymentsAdapter = new PaymentsAdapter();
        rvPayments.setAdapter(paymentsAdapter);

        btnSale = findViewById(R.id.btn_sale);
        btnSale.setOnClickListener(v -> presenter.onNewSaleClick());


    }

    private void openDrawer() {
        drawerLayout.openDrawer(navigationDrawer);
    }

    private boolean navigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        Toast.makeText(this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    };

    @Override
    public Activity getActivity() {
        return PaymentsActivity.this;
    }
    //Barra de progreso
    @Override
    public void showProgressBar() {//Mostrar barra de progreso
        piWaiting.setVisibility(View.VISIBLE);
    }
    @Override
    public void hideProgressBar() {//Ocultar barra de progreso
        piWaiting.setVisibility(View.GONE);
    }
    //Mostrar nueva venta en ventana
    @Override
    public void showNewSaleActivity() {
        Intent intent = (new Intent(PaymentsActivity.this, NewSaleActivity.class));
        intent.putExtra("Client", "Viviana");
        startActivity(intent);
    }

    @Override
    public void showPaymentsInfo(List<PaymentsMVP.PaymentsInfo> paymentsInfo) {
        paymentsAdapter.setData(paymentsInfo);
        //Toast.makeText(PaymentsActivity.this, "Datos cargados", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showGeneralMessage(String credenciales_invalidas) {

    }
}