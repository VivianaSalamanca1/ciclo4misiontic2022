package com.example.ventasdomiciliog1.presenter;

import android.widget.Toast;

import com.example.ventasdomiciliog1.mvp.NewSaleMVP;
import com.example.ventasdomiciliog1.view.NewSaleActivity;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class NewSalePresenter implements NewSaleMVP.Presenter {

    private NewSaleMVP.View view;
    //private NewSaleActivity newSaleActivity;

    //private Object NewSaleActivity;

    public NewSalePresenter(NewSaleMVP.View view) {
        this.view = view;
    }

    @Override
    public void saveSale() {
        NewSaleMVP.NewSaleInfo newSaleInfo = view.getNewSaleInfo();
        boolean error = false;
        //Validaciones de los campos del Login
        if (newSaleInfo.getClient().trim().isEmpty()) {
            view.messageIncomplete();
            error = true;
        }
        if (newSaleInfo.getAddress().trim().isEmpty()) {
            view.messageIncomplete();
            error = true;
        }
        if (newSaleInfo.getAmount().trim().isEmpty()) {
            view.messageIncomplete();
            error = true;
        }
        if (newSaleInfo.getNumber().trim().isEmpty()) {
            view.messageIncomplete();
            error = true;
        }
        if (newSaleInfo.getPeriodicity().trim().isEmpty()) {
            view.messageIncomplete();
            error = true;
        }
        if (newSaleInfo.getPart().trim().isEmpty()) {
            view.messageIncomplete();
            error = true;
        }
        //if (newSaleInfo.getDate().trim().isEmpty()) { //TODO: Revisar por ser otro tipo de campo
        //view.messageIncomplete();
        //error = true;
        //}
        if (!error) {
            view.clearDate();
            view.message();
        }
    }
}
