package com.example.ventasdomiciliog1.presenter;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.ventasdomiciliog1.model.LoginInteractor;
import com.example.ventasdomiciliog1.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {
    //Atributos
    private final String AUTH_PREFERENCE = "authentication";
    private final String LOGGED_KEY = "logged";

    private LoginMVP.View view;
    private LoginMVP.Model model;
    //Constructor
    public LoginPresenter(LoginMVP.View view) {
        this.view = view;
        this.model= new LoginInteractor(); //Falta instanciar el modelo a trabajar
    }

    @Override
    public void isLogged() {
        SharedPreferences preferences= view.getActivity()
                .getSharedPreferences(AUTH_PREFERENCE, Context.MODE_PRIVATE);
        boolean isLogged = preferences.getBoolean(LOGGED_KEY, false);
        if(isLogged){
            view.openPaymentsActivity();
        }
    }

    @Override
    public void loginwithEmail() {
        boolean error = false;

        view.showEmailError("");
        view.showPasswordError("");
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();

        //Validaciones de los campos del Login
        if (loginInfo.getEmail().trim().isEmpty()) {
            view.showEmailError("Correo electrónico es obligatorio");
            error = true;
        } else if (!isvalidateEmail(loginInfo.getEmail().trim())) {
            view.showEmailError("Correo electrónico no es valido");
            error = true;
        }
        if (loginInfo.getPassword().trim().isEmpty()) {
            view.showPasswordError("Contraseña es obligatoria");
            error = true;
        } else if (!isPasswordValid(loginInfo.getPassword().trim())) {
            view.showPasswordError("Contraseña no cumple los criterios de seguridad");
            error = true;
        }

        //Validaciones usuario-contraseña correctos
        if (!error) {
            view.startWaiting();
            new Thread(() -> { //Manejar en segundo plano para no bloquear la aplicación
                model.validationCredentials(
                        loginInfo.getEmail().trim(),
                        loginInfo.getPassword().trim(),
                        new LoginMVP.Model.ValidateCredentialsCallback() {
                            @Override
                            public void onSuccess() {
                                SharedPreferences preferences= view.getActivity()
                                        .getSharedPreferences(AUTH_PREFERENCE, Context.MODE_PRIVATE);
                                preferences.edit()
                                        .putBoolean(LOGGED_KEY, true).apply();


                                //Ejecutarlo en el mismo hilo de la aplicación
                                view.getActivity().runOnUiThread(() -> {
                                    view.stopWaiting();
                                    view.openPaymentsActivity();
                                });
                            }
                            @Override
                            public void onFailure(String error) {
                                view.getActivity().runOnUiThread(() -> {
                                    view.stopWaiting();
                                    view.showGeneralMessage("Credenciales invalidas");
                                });
                            }
                        });
            }).start();
        }
    }

       /* //Si cumple las validaciones, que abra la siguiente ventana
        if (!error) {
            view.openPaymentsActivity();
        }
        else {
            view.showGeneralMessage("Verifique los datos");
        }*/

    //Validación del correo
    private boolean isvalidateEmail(String email) {
        return email.contains("@") && email.endsWith(".com");
    }
    //Validación de contraseña
    private boolean isPasswordValid(String password) {
        return password.length()>=8;
    }

    @Override
    public void loginwithFacebook() {
    }
    @Override
    public void loginwithGmail() {
    }
}
