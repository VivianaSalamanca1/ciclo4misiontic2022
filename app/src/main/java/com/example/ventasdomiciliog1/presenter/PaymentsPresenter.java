package com.example.ventasdomiciliog1.presenter;

import android.view.View;

import com.example.ventasdomiciliog1.model.PaymentsInteractor;
import com.example.ventasdomiciliog1.mvp.PaymentsMVP;

import java.util.List;

public class PaymentsPresenter implements PaymentsMVP.Presenter {
    //Atributos
    private PaymentsMVP.View view;
    private PaymentsMVP.Model model;

    //Constructor
    public PaymentsPresenter(PaymentsMVP.View view) {
        this.view = view; //La interfaz vista se recibe por el constructor
        this.model = new PaymentsInteractor();
    }

    //Métodos de los eventos
    @Override
    public void loadPayments() {
            view.showProgressBar();
            view.hideProgressBar();
            new Thread(() -> {
                model.loadPayments(new PaymentsMVP.Model.LoadPaymentsCallback() {
                    @Override
                    public void showPaymentsInfo(List<PaymentsMVP.PaymentsInfo> paymentsInfo) {
                        view.getActivity().runOnUiThread(() -> {
                            view.showPaymentsInfo(paymentsInfo);
                            view.showProgressBar();
                        });
                    }
                    public void hideProgressBar() {//Ocultar barra de progreso
                        view.getActivity().runOnUiThread(() -> {
                            view.hideProgressBar();
                            view.showGeneralMessage("Datos cargados");
                        });
                    }
                });
            }).start();
        }
            @Override
            public void onNewSaleClick(){ //Botón de nueva venta
                view.showNewSaleActivity();
            }
        }

